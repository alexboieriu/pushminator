var express = require ('express');

var app = express();

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers','Content-Type')
    res.header('Content-Type', 'text/event-stream');

    if ('OPTIONS' == req.method) {
        res.sendStatus(200, '');
    } else {
        next();
    }
};

var router = express.Router();
router.get('/data-stream', function(req, res){
  console.log('whooa, requested!');
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  var id = (new Date()).toLocaleTimeString();

  // Sends a SSE every 5 seconds on a single connection.
  setInterval(function() {
    constructSSE(res, id, (new Date()).toLocaleTimeString());
  }, 10000);

  constructSSE(res, id, (new Date()).toLocaleTimeString());
});

function constructSSE(res, id, data) {
  res.write('id: ' + id + '\n');
  res.write("data: " + data + '\n\n');
}

app.listen(3002);
app.use(allowCrossDomain);
app.use('/', router);

console.log('info', 'Server started at port ' + 3002);

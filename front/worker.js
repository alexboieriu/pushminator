/*console.log('setup worker');
}
*/
console.log('activated');
if (!!EventSource) {
  var source = new EventSource('http://localhost:3002/data-stream');
  console.log('alright, lets try to stream');
  source.addEventListener('message', function(e) {
    console.log('received');
    //new Notification(JSON.stringify(e.data));
    self.registration.showNotification("New notification from service worker", {
      body: JSON.stringify(e.data)
    })
  }, false);

  source.addEventListener('open', function(e) {
    console.log('connection opened')
  }, false);

  source.addEventListener('error', function(e) {
    if (e.readyState == EventSource.CLOSED) {
      console.log('connection closed')
    }
  }, false);
} else {
  // Result to xhr polling :(
}
this.addEventListener('fetch', function(event) {
  console.log("worker fetch");
});

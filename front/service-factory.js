registerServiceWorker = function() {
  // Check that service workers are supported
  if ('serviceWorker' in navigator) {
    worker = navigator.serviceWorker.register('http://localhost:3001/worker.js')
    .catch(() => {
      console.log(
        'Unable to Register SW',
        'Sorry this demo requires a service worker to work and it ' +
        'was didn\'t seem to install - sorry :('
      );
    });

    worker.then(function(worker){
      //debugger;
    })
  } else {
    console.log(
      'Service Worker Not Supported',
      'Sorry this demo requires service worker support in your browser. ' +
      'Please try this demo in Chrome or Firefox Nightly.'
    );
  }
}

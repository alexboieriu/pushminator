var static = require('node-static');
//
// Create a node-static server instance to serve the './' folder as resources
//
var file = new static.Server('./');


require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        //
        // Serve files!
        //
        file.serve(request, response);
    }).resume();
}).listen(3001);
console.log('start listening on 3001');
